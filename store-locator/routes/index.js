﻿var express = require('express');
var router = express.Router();
var path = require('path');
var mongoose = require('mongoose');

//Connect to mongodb
mongoose.connect('mongodb://localhost:27017/');
var storeSchema = mongoose.Schema({
    name: String,
    pincode: String,
    address: String
   
});

var StoreModel = mongoose.model('store', storeSchema);

StoreModel.find().exec(function (error, results) {
    if (results.length === 0) {
        StoreModel.create({ name: "Centre bakers", pincode: '411034', address: "8500 Beverly Blvd" });
        StoreModel.create({ name: "Live and laugh", pincode: '411045', address: "189 The Grove Dr" });
        StoreModel.create({ name: "Beverly Cente", pincode: '490143', address: "100 Citadel Drive" });
        StoreModel.create({ name: "The Grove", pincode: '411052', address: "433 st clara" });
        StoreModel.create({ name: "The Last Bookstore", pincode: '411045', address: "873 Peter Avenues" });
        StoreModel.create({ name: "Citadel Outlets", pincode: '475001', address: "100 Citadel Drive" });
        StoreModel.create({ name: "Meltdown Comics and Collectibles", pincode: '411052', address: "7522 Sunset Blvd" });
        StoreModel.create({ name: "Sweet E's Bake Shop", pincode: '490143', address: "8215 W 3rd" });
        StoreModel.create({ name: "Empty Vase Florist", pincode: '411045', address: "9033 Santa Monica Blvd" });
        StoreModel.create({ name: "El Maestro Bicycle Shop", pincode: '490143', address: "806 S Main St" });
        StoreModel.create({ name: "Nick's Coffee Shop & De", pincode: '411052', address: "8536 W Pico Blvd" });
        StoreModel.create({ name: "The Giftshop at the Cathedral of Our Lad", pincode: '411052', address: "555 W Temple S" });
        StoreModel.create({ name: "Mike's Smoke Shop & Vapor Bar", pincode: '411052', address: "6618 Hollywood Blvd" });
        StoreModel.create({ name: "Ignite Smoke & Vape Sh", pincode: '475001', address: "6630 Hollywood Blvd" });
        StoreModel.create({ name: "Pico Union Pawn Shop", pincode: '411045', address: "4579 W Pico Blvd" });
        StoreModel.create({ name: "Bikecology", pincode: '490143', address: "9006 W Pico Blvd" });
        StoreModel.create({ name: "Mr. Steve's Pawnsho", pincode: '475001', address: "800 S Western A" });
        StoreModel.create({ name: "Kinokuniya Los Angeles", pincode: '490143', address: "123 Astronaut E S Onizuka St" });
        StoreModel.create({ name: "Simon's Camera Inc", pincode: '475001', address: "1213 Highland Av" });
        StoreModel.create({ name: "Bricks and Scones", pincode: '411052', address: "403 N Larchmont Blvd" });
        StoreModel.create({ name: "Lemon Frog Shop", pincode: '411045', address: "1202 N Alvarado St" });
        StoreModel.create({ name: "Water Tower Place", pincode: '490143', address: "835 N Michigan Ave" });
        StoreModel.create({ name: "900 North Michigan Shops", pincode: '411052', address: "900 N Michigan Ave" });
        StoreModel.create({ name: "Roscoe Village Bikes", pincode: '475001', address: "2016 W Roscoe St" });
        StoreModel.create({ name: "The Bike Lane", pincode: '411045', address: "2130 N Milwaukee Ave" });
        StoreModel.create({ name: "Comrade Cycles", pincode: '411052', address: "1908 W Chicago Ave" });
        StoreModel.create({ name: "Kozy's Cyclery Megastore", pincode: '475001', address: "3255 N Milwaukee Ave" });
    }
});

/* GET home page. */
router.get('/', function (req, res) {
    res.render('./theme/index.html', { title: 'Express' });
});

router.get('/api/stores', function (req, res) {
    StoreModel.find().exec(function (err, stores) {
        if (err) {
            res.send(err)
        }
        res.json(stores);
    });
});

router.get('/api/stores/:pincode', function (req, res) {
    StoreModel.find().where({ 'pincode': req.params.pincode }).exec(function (err, stores) {
        if (err) {
            res.send(err);
        }
        res.json(stores);
    })
});

module.exports = router;