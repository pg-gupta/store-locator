﻿# store-locator
This app helps you to locate nearby store based on the pincode provided in the search textbox

# prerequisites
Before you run the app make sure following is installed on your machine
1. NodeJs: https://nodejs.org/en/ 
2. Git: https://git-scm.com/download/win


# Steps to run the app
	
	* Run Mongo
	1. Open command prompt in Admin mode and navigate to the 'data' folder of the project.
	2. Hit the command 'mongod --dbpath "db"'
	3. This will start mongo on port 27017.

	* Run the app
	1. Open command prompt and open the project path. (cd "../store-locator/store-locator").	
	2. Hit the command 'npm install'. This will install all the dependencies and packages in package.json file.
	3. Hit the command 'node app.js' to run the project.
	4. Open browser and hit "localhost:3000".
	5. The webapp will show all the stores by default (scrolling down).
	6. In the search box enter a pincode eg: 411052.This will list all the stores which are in this region.




