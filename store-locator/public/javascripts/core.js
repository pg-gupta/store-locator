﻿var locateStore = angular.module('locateStores', []);

locateStore.controller('mainController', function ($scope, $http) {
    $scope.formData = {};
    
    // when landing on the page, get all stores
    $scope.init = function () {
        $http.get('/api/stores')
        .success(function (data) {
            $scope.stores = data;
            console.log(data);
        })
        .error(function (data) {
            console.log('Error: ' + data);
        });
    };
    
    // locate stores by pincode
    $scope.locate = function () {
        if ($scope.pincode != "" && $scope.pincode != null && $scope.pincode != undefined) {
            $http.get('/api/stores/' + $scope.pincode)
            .success(function (data) {
                $scope.stores = data;
            })
            .error(function (err) {
                $scope.error = err;
            });
        }        
        else {
            $scope.init();
        }
     
    };
});
